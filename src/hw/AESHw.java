package hw;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;

public class AESHw {
	 final int AES_KEY_SIZE_128 = 128;
	 String pathName = "/Users/OhDaeKyoung/eclipse-workspace/hw/";
	 SecretKeySpec secretKey;
	 Cipher cipher;
	
    public void createScretKey(String sKey) throws Exception{
    		//128bit 비밀키 생성 (AES알고리즘의 ECB 모드를 위해 )
    		byte[] key = null;
    		key = sKey.getBytes();
    		//입력 받은 키값을 사용하여 128bit의 byte 문자열로 바꾸어준다 
    		key = Arrays.copyOf(key, AES_KEY_SIZE_128/8);
    		secretKey = new SecretKeySpec(key,"AES");
      	//ECB모드, PKCS5Padding 모드를 위한 AES알고리즘에 쓰일 cipher object 할당 받는다.
      	cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
    }
    
    public void doEncryption(String path) throws Exception { // 암호화 수행
    		//입력받은 파일명의 파일을 열어 평서문 문자열을 읽는
    		File f = new File(pathName+path);
    		BufferedReader in = new BufferedReader(new FileReader(f));
        String message;
        message = in.readLine();
        in.close();
        //읽어온 평서문 출력 
        System.out.println("saved file message : " + message);
    		//cipher object를 Encrytion 모드로 초기화한다 (비밀키를 사용하여)
    	 	cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    	 	//입력받은 String의 유니코드 문자열을 인자로 지정된 캐릭터셋의 바이트 배열로 반환하여,
    	    // cipher . doFinal을 이용해 AES 알고리즘을 사용해 암호화 문자열로 치환한다 
 	    byte[] encrypted = cipher.doFinal(message.getBytes());
 	    //암호화된문자열을 Data.txt파일에 저장한다 
 	    //Byte 단위이기때문에 FileOutputStream 을 사용한다 
 	    FileOutputStream out = new FileOutputStream(f);
 	    out.write(encrypted);
 	    out.close();
 	    //암호화된 문자열 출력 (결과값 확인)
 	    System.out.println("encrypted string: " + encrypted);
    }
    
	public void doDecryption(String path) throws Exception {
		//입력 받은 경로의 파일을 연다.
		File f = new File(pathName+path);
		//암호화된 문자열을 data.txt 파일에서 읽어온다 
  		FileInputStream input = new FileInputStream(f);
  		//Byte 단위이기때문에 FileInputStream 을 사용하며 FileInputStream 의 available 함수를 사용하여 
  		//저장된 암호화 문자열의 길이만큼 동적 할당받는다 
  		byte[] encrypted = new byte[input.available()];
  		input.read(encrypted);
  		input.close();
  		//읽어온 암호화 문자열 출력 
  		System.out.println("encrypted string: " + encrypted);
  		
		//cipher object를 Decrytion 모드로 초기화한다 (비밀키를 사용하여)
	    cipher.init(Cipher.DECRYPT_MODE, secretKey);
	    // cipher . doFinal을 이용해 AES 알고리즘을 사용해 암호화 문자열을 복호화 한다 
	    byte[] original = cipher.doFinal(encrypted);
	    //Byte 배열로 변환된 복호화 문자열을 String 객체로 치환한
	    String originalString = new String(original);
	    // 복호화 문자열 출력(결과값 확인)
	    System.out.println("Decrypted string: " + originalString);
	  }
}
