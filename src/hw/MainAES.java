package hw;
import java.util.Scanner;
public class MainAES {
	public static void main(String[] args) {
		//1 을 입력받으면 암호화, 2를 입력받으면 복호화 
		System.out.println("1. encrytion\n 2.decryption");
		String key;
		String path;
		int n;
		// 암호화 복호화 기능을 하는 AESHw 클래스에 대한 인스턴스 생성 
		AESHw aes = new AESHw();
		Scanner scn = new Scanner(System.in);
		// key 값과 파일명을 입력받는
		key = scn.nextLine();
		path = scn.nextLine();
		// 1 or 2를 입력받는다 
		n = scn.nextInt();
		scn.close();
		try {
			if(n == 1) { //암호화 
				//암호화에 필요한 키를 생성하는 함수 호출 
				aes.createScretKey(key);
				//암호화 기능을 하는 함수 호출 
				aes.doEncryption(path);
			}
			else if(n==2) {
				//복호화에 필요한 키를 생성하는 함수 호출 
				aes.createScretKey(key);
				//복호화 기능을 하는 함수 호출 
				aes.doDecryption(path);
			}
			else {// 1,2 가 아닌 다른 값을 받을때 에러
				System.out.println("input error");
			}
		} catch (Exception e) {
			//모든 과정에서 발생하는 예외처리를 받아 에러를 출력한다 
			System.out.println(e.getMessage());
		}
	}
}
